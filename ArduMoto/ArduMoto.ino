#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define XPOS 0
#define YPOS 1
#define DELTAY 2


int input = A0;
int maxAdc = 4095;

void adc_setup()
{
	pinMode(input, INPUT);
	// Setup all registers
	pmc_enable_periph_clk(ID_ADC); // To use peripheral, we must enable clock distributon to it
	adc_init(ADC, SystemCoreClock, ADC_FREQ_MAX, ADC_STARTUP_FAST); // initialize, set maximum posibble speed
	adc_disable_interrupt(ADC, 0xFFFFFFFF);
	adc_set_resolution(ADC, ADC_12_BITS);
	adc_configure_power_save(ADC, 0, 0); // Disable sleep
	adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1); // Set timings - standard values
	adc_set_bias_current(ADC, 1); // Bias current - maximum performance over current consumption
	adc_stop_sequencer(ADC); // not using it
	adc_disable_tag(ADC); // it has to do with sequencer, not using it
	adc_disable_ts(ADC); // deisable temperature sensor
	adc_disable_channel_differential_input(ADC, ADC_CHANNEL_7);
	adc_configure_trigger(ADC, ADC_TRIG_SW, 1); // triggering from software, freerunning mode
	adc_disable_all_channel(ADC);
	adc_enable_channel(ADC, ADC_CHANNEL_7); // just one channel enabled
	adc_start(ADC);
}


void setup() {
	Serial.begin(9600);
	adc_setup();

	// by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
	display.display();
	display.clearDisplay();

	// text display tests
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0, 0);
	display.println("Hello, world!");
	display.setTextColor(BLACK, WHITE); // 'inverted' text
	display.println(3.141592);
	display.setTextSize(2);
	display.setTextColor(WHITE);
	display.print("0x"); display.println(0xDEADBEEF, HEX);
	display.display();

}


void loop() {
	display.setTextSize(2);
	while (1)
	{
		delay(1000);
		PIO_Set(PIOB, PIO_PB27B_TIOB0);
		while ((adc_get_status(ADC) & ADC_ISR_DRDY) != ADC_ISR_DRDY)
		{
		}; //Wait for end of conversion
		PIO_Clear(PIOB, PIO_PB27B_TIOB0);
		int val = adc_get_latest_value(ADC);

		float valV = (3.3 * val) / 4095.0;

		display.clearDisplay();
		display.setCursor(0, 0);
		display.print("A0: ");
		display.print(valV);
		display.display();
	}
}

void testscrolltext(void) {
	display.setTextSize(2);
	display.setTextColor(WHITE);
	display.setCursor(10, 0);
	display.clearDisplay();
	display.println("scroll");
	display.display();
	delay(1);

	display.startscrollright(0x00, 0x0F);
	delay(2000);
	display.stopscroll();
	delay(1000);
	display.startscrollleft(0x00, 0x0F);
	delay(2000);
	display.stopscroll();
	delay(1000);
	display.startscrolldiagright(0x00, 0x07);
	delay(2000);
	display.startscrolldiagleft(0x00, 0x07);
	delay(2000);
	display.stopscroll();
}